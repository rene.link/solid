package com.link_intersystems.solid.srp.violation;

import com.link_intersystems.solid.srp.AbstractEmployeeTest;
import com.link_intersystems.solid.srp.Employee;

public class EmployeeImplTest extends AbstractEmployeeTest {

	@Override
	protected Employee createEmployee() {
		return new EmployeeImpl();
	}

}
