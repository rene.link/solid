package com.link_intersystems.solid.srp;

import static java.time.LocalDate.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static org.junit.Assert.assertEquals;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import com.link_intersystems.solid.srp.WorkLog;

public class WorkLogTest {

	private WorkLog workLog;

	@Before
	public void setup() {
		LocalDate day = LocalDate.now();
		LocalTime start = LocalTime.parse("08:35");
		LocalTime end = LocalTime.parse("17:45");
		Duration breakTime = Duration.of(30, MINUTES);

		workLog = new WorkLog(day, start, end);
		workLog.setBreakDuration(breakTime);
	}

	@Test
	public void workLog() {
		new WorkLog(now(), LocalTime.NOON, LocalTime.MAX);
	}

	@Test(expected = IllegalArgumentException.class)
	public void wrongWorkLog() {
		new WorkLog(now(), LocalTime.MAX, LocalTime.MIN);
	}

	@Test
	public void getDay() {
		LocalDate day = now();
		WorkLog workLog = new WorkLog(day, LocalTime.NOON, LocalTime.MAX);

		LocalDate day2 = workLog.getDay();
		assertEquals(day, day2);
	}


	@Test
	public void breakDuration() {
		Duration breakDuration = workLog.getBreakDuration();
		assertEquals(Duration.of(30, MINUTES), breakDuration);
	}

	@Test
	public void getWorkDuration() {
		Duration workDuration = workLog.getWorkDuration();
		assertEquals(Duration.of(520, MINUTES), workDuration);
	}

}
