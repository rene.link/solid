package com.link_intersystems.solid.srp;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TimeSheetTest {

	private TimeSheet timeSheet;

	@Before
	public void setup() {
		timeSheet = new TimeSheet();
	}

	@Test
	public void addWorkLog() {
		WorkLog workLog1 = createWorkLog("2019-05-01", "08:15", "16:30", "PT30M");
		WorkLog workLog2 = createWorkLog("2019-05-02", "08:00", "17:30", "PT45M");

		timeSheet.addWorkLog(workLog1);
		List<WorkLog> workLogs = timeSheet.getWorkLogs(LocalDate.parse("2019-05-01"), LocalDate.parse("2019-05-04"));
		Assert.assertEquals(1, workLogs.size());
		Assert.assertEquals(workLog1, workLogs.get(0));

		timeSheet.addWorkLog(workLog2);
		workLogs = timeSheet.getWorkLogs(LocalDate.parse("2019-05-01"), LocalDate.parse("2019-05-04"));
		Assert.assertEquals(2, workLogs.size());
		Assert.assertEquals(workLog2, workLogs.get(1));
	}

	@Test
	public void getWorkLogs() {
		WorkLog workLog1 = createWorkLog("2019-05-01", "08:15", "16:30", "PT30M");
		WorkLog workLog2 = createWorkLog("2019-05-02", "08:00", "17:30", "PT45M");
		WorkLog workLog3 = createWorkLog("2019-05-03", "08:00", "18:30", "PT60M");

		timeSheet.addWorkLog(workLog1);
		timeSheet.addWorkLog(workLog2);
		timeSheet.addWorkLog(workLog3);

		List<WorkLog> workLogs = timeSheet.getWorkLogs(LocalDate.parse("2019-05-02"), LocalDate.parse("2019-05-03"));
		Assert.assertEquals(Arrays.asList( workLog2), workLogs);
	}

	private WorkLog createWorkLog(String day, String start, String end, String breakTime) {
		WorkLog workLog = new WorkLog(LocalDate.parse(day), LocalTime.parse(start), LocalTime.parse(end));
		workLog.setBreakDuration(Duration.parse(breakTime));
		return workLog;
	}

}
