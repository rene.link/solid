package com.link_intersystems.solid.srp.data_separation.facade;

import com.link_intersystems.solid.srp.AbstractEmployeeTest;
import com.link_intersystems.solid.srp.Employee;
import com.link_intersystems.solid.srp.data_separation.EmployeeData;

public class EmployeeFacadeTest extends AbstractEmployeeTest {

	@Override
	protected Employee createEmployee() {
		EmployeeData employeeData = new EmployeeData();
		return new EmployeeFacade(employeeData);
	}

}
