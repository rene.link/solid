package com.link_intersystems.solid.srp;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public abstract class AbstractEmployeeTest {

	private Employee employee;

	@Before
	public void setup() {
		employee = createEmployee();
		addWorkLog("2019-05-01", "08:15", "16:30", "PT30M");
		addWorkLog("2019-05-02", "08:00", "17:30", "PT45M");
		addWorkLog("2019-05-03", "08:00", "18:30", "PT60M");
	}

	protected abstract Employee createEmployee();

	private void addWorkLog(String day, String start, String end, String breakTime) {
		WorkLog workLog = new WorkLog(LocalDate.parse(day), LocalTime.parse(start), LocalTime.parse(end));
		workLog.setBreakDuration(Duration.parse(breakTime));
		employee.addWorkLog(workLog);
	}

	@Test
	public void calculatePay() {
		BigDecimal salary = employee.calculatePay(LocalDate.parse("2019-05-01"), LocalDate.parse("2019-05-04"));
		Assert.assertEquals(new BigDecimal("1062.50"), salary);
	}

	@Test
	public void reportOvertime() {
		BigDecimal overtime = employee.reportOvertime(LocalDate.parse("2019-05-01"), LocalDate.parse("2019-05-04"));
		Assert.assertEquals(new BigDecimal("2.25"), overtime);
	}

}
