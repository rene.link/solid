package com.link_intersystems.solid.srp;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class TimeSheet {

	private SortedMap<LocalDate, WorkLog> workLogs = new TreeMap<>();

	public void addWorkLog(WorkLog workLog) {
		workLogs.put(workLog.getDay(), workLog);
	}

	public List<WorkLog> getWorkLogs(LocalDate from, LocalDate to) {
		SortedMap<LocalDate, WorkLog> workLogsFrom = workLogs.tailMap(from);
		SortedMap<LocalDate, WorkLog> workLogsBetween = workLogsFrom.headMap(to);
		return new ArrayList<WorkLog>(workLogsBetween.values());
	}
}
