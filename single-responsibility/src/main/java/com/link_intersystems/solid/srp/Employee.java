package com.link_intersystems.solid.srp;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface Employee {

	BigDecimal calculatePay(LocalDate from, LocalDate to);

	BigDecimal reportOvertime(LocalDate from, LocalDate to);

	void addWorkLog(WorkLog workLog);

}