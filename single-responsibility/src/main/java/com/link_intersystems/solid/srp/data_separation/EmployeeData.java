package com.link_intersystems.solid.srp.data_separation;

import java.math.BigDecimal;

import com.link_intersystems.solid.srp.TimeSheet;
import com.link_intersystems.solid.srp.WorkLog;

public class EmployeeData {


	private BigDecimal hourlyCompensation = new BigDecimal("40");
	private BigDecimal overtimeCompensation = new BigDecimal("50");

	private TimeSheet timeSheet = new TimeSheet();

	public void addWorkLog(WorkLog workLog) {
		timeSheet.addWorkLog(workLog);
	}

	public TimeSheet getTimeSheet() {
		return timeSheet;
	}

	public BigDecimal getHourlyCompensation() {
		return hourlyCompensation;
	}

	public BigDecimal getOvertimeCompensation() {
		return overtimeCompensation;
	}
}
