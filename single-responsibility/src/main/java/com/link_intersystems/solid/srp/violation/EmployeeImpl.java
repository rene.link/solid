package com.link_intersystems.solid.srp.violation;

import static java.time.temporal.ChronoUnit.HOURS;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

import com.link_intersystems.solid.srp.Employee;
import com.link_intersystems.solid.srp.TimeSheet;
import com.link_intersystems.solid.srp.WorkLog;

public class EmployeeImpl implements Employee {

	private static final Duration REGULAR_WORK_DURATION = Duration.of(8, HOURS);
	private static final BigDecimal MINS_PER_HOUR = new BigDecimal(60);

	private BigDecimal hourlyCompensation = new BigDecimal("40");
	private BigDecimal overtimeCompensation = new BigDecimal("50");

	private TimeSheet timeSheet = new TimeSheet();

	@Override
	public BigDecimal calculatePay(LocalDate from, LocalDate to) {
		BigDecimal salary = BigDecimal.ZERO;

		BigDecimal regularHours = getRegularHours(from, to);
		BigDecimal regularHoursSalary = regularHours.multiply(hourlyCompensation);
		salary = salary.add(regularHoursSalary);

		BigDecimal overtimeHours = getOvertimeHours(from, to);
		BigDecimal overtimeSalary = overtimeHours.multiply(overtimeCompensation);
		salary = salary.add(overtimeSalary);

		return salary;
	}

	@Override
	public BigDecimal reportOvertime(LocalDate from, LocalDate to) {
		BigDecimal overtime = getOvertimeHours(from, to);
		return overtime;
	}

	private BigDecimal getRegularHours(LocalDate from, LocalDate to) {
		BigDecimal regularHours = BigDecimal.ZERO;

		List<WorkLog> workLogs = timeSheet.getWorkLogs(from, to);

		for (WorkLog workLog : workLogs) {
			Duration workDuration = workLog.getWorkDuration();
			Duration regularWorkDuration = toRegularWorkDuration(workDuration);

			long regularWorkMins = regularWorkDuration.toMinutes();
			BigDecimal workLogHours = new BigDecimal(regularWorkMins).divide(MINS_PER_HOUR);

			regularHours = regularHours.add(workLogHours);
		}

		return regularHours;
	}

	private Duration toRegularWorkDuration(Duration workDuration) {
		if (workDuration.compareTo(REGULAR_WORK_DURATION) > 0) {
			workDuration = REGULAR_WORK_DURATION;
		}
		return workDuration;
	}

	private BigDecimal getOvertimeHours(LocalDate from, LocalDate to) {
		List<WorkLog> workLogs = timeSheet.getWorkLogs(from, to);

		BigDecimal overtimeHours = BigDecimal.ZERO;

		for (WorkLog workLog : workLogs) {
			Duration workDuration = workLog.getWorkDuration();
			Duration overtimeDuration = getOvertimeDuration(workDuration);
			long minutes = overtimeDuration.toMinutes();
			BigDecimal workLogHours = new BigDecimal(minutes).divide(new BigDecimal(60));
			overtimeHours = overtimeHours.add(workLogHours);
		}

		return overtimeHours;
	}

	private Duration getOvertimeDuration(Duration workDuration) {
		Duration overtimeDuration = workDuration.minus(REGULAR_WORK_DURATION);
		if (overtimeDuration.isNegative()) {
			overtimeDuration = Duration.ZERO;
		}
		return overtimeDuration;
	}

	@Override
	public void addWorkLog(WorkLog workLog) {
		timeSheet.addWorkLog(workLog);
	}

}
