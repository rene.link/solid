package com.link_intersystems.solid.srp;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class WorkLog {

	private LocalDate day;
	private LocalTime start;
	private LocalTime end;
	private Duration breakDuration = Duration.ZERO;

	public WorkLog(LocalDate day, LocalTime start, LocalTime end) {
		this.day = Objects.requireNonNull(day);
		this.start = Objects.requireNonNull(start);
		this.end = Objects.requireNonNull(end);

		if (this.start.compareTo(this.end) >= 0) {
			throw new IllegalArgumentException("start must be before end");
		}
	}

	public LocalDate getDay() {
		return day;
	}

	public Duration getWorkDuration() {
		Duration presenceDuration = Duration.between(start, end);
		Duration workDuration = presenceDuration.minus(breakDuration);
		return workDuration;
	}

	public void setBreakDuration(Duration breakDuration) {
		this.breakDuration = Objects.requireNonNull(breakDuration);
	}

	public Duration getBreakDuration() {
		return breakDuration;
	}

}
