package com.link_intersystems.solid.srp.data_separation.facade;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.link_intersystems.solid.srp.Employee;
import com.link_intersystems.solid.srp.WorkLog;
import com.link_intersystems.solid.srp.data_separation.EmployeeData;
import com.link_intersystems.solid.srp.data_separation.OvertimeReporter;
import com.link_intersystems.solid.srp.data_separation.PayCalculator;

public class EmployeeFacade implements Employee {

	private EmployeeData employeeData;

	public EmployeeFacade(EmployeeData employeeData) {
		this.employeeData = employeeData;
	}

	@Override
	public BigDecimal calculatePay(LocalDate from, LocalDate to) {
		PayCalculator payCalculator = new PayCalculator(employeeData);
		return payCalculator.calculatePay(from, to);
	}

	@Override
	public BigDecimal reportOvertime(LocalDate from, LocalDate to) {
		OvertimeReporter overtimeReporter = new OvertimeReporter(employeeData);
		return overtimeReporter.reportOvertime(from, to);
	}

	@Override
	public void addWorkLog(WorkLog workLog) {
		employeeData.addWorkLog(workLog);
	}

}
