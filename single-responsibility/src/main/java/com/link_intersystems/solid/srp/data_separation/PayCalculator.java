package com.link_intersystems.solid.srp.data_separation;

import static java.time.temporal.ChronoUnit.HOURS;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

import com.link_intersystems.solid.srp.TimeSheet;
import com.link_intersystems.solid.srp.WorkLog;

public class PayCalculator {

	private static final Duration REGULAR_WORK_DURATION = Duration.of(8, HOURS);
	private static final BigDecimal MINS_PER_HOUR = new BigDecimal(60);

	private EmployeeData employeeData;

	public PayCalculator(EmployeeData employeeData) {
		this.employeeData = employeeData;
	}

	public BigDecimal calculatePay(LocalDate from, LocalDate to) {
		BigDecimal salary = BigDecimal.ZERO;

		BigDecimal regularHours = getRegularHours(employeeData, from, to);
		BigDecimal hourlyCompensation = employeeData.getHourlyCompensation();
		BigDecimal regularHoursSalary = regularHours.multiply(hourlyCompensation);
		salary = salary.add(regularHoursSalary);

		BigDecimal overtimeHours = getOvertimeHours(employeeData, from, to);
		BigDecimal overtimeCompensation = employeeData.getOvertimeCompensation();
		BigDecimal overtimeSalary = overtimeHours.multiply(overtimeCompensation);
		salary = salary.add(overtimeSalary);

		return salary;
	}

	private BigDecimal getRegularHours(EmployeeData employeeData, LocalDate from, LocalDate to) {
		BigDecimal regularHours = BigDecimal.ZERO;

		TimeSheet timeSheet = employeeData.getTimeSheet();
		List<WorkLog> workLogs = timeSheet.getWorkLogs(from, to);

		for (WorkLog workLog : workLogs) {
			Duration workDuration = workLog.getWorkDuration();
			Duration regularWorkDuration = toRegularWorkDuration(workDuration);

			long regularWorkMins = regularWorkDuration.toMinutes();
			BigDecimal workLogHours = new BigDecimal(regularWorkMins).divide(MINS_PER_HOUR);

			regularHours = regularHours.add(workLogHours);
		}

		return regularHours;
	}

	private BigDecimal getOvertimeHours(EmployeeData employeeData, LocalDate from, LocalDate to) {
		TimeSheet timeSheet = employeeData.getTimeSheet();
		List<WorkLog> workLogs = timeSheet.getWorkLogs(from, to);

		BigDecimal regularHours = BigDecimal.ZERO;

		for (WorkLog workLog : workLogs) {
			Duration workDuration = workLog.getWorkDuration();
			Duration overtimeDuration = getOvertimeDuration(workDuration);
			long minutes = overtimeDuration.toMinutes();
			BigDecimal workLogHours = new BigDecimal(minutes).divide(MINS_PER_HOUR);
			regularHours = regularHours.add(workLogHours);
		}

		return regularHours;
	}

	private Duration getOvertimeDuration(Duration regularWorkDuration) {
		Duration overtimeDuration = regularWorkDuration.minus(REGULAR_WORK_DURATION);
		if (overtimeDuration.isNegative()) {
			overtimeDuration = Duration.ZERO;
		}
		return overtimeDuration;
	}

	private Duration toRegularWorkDuration(Duration workDuration) {
		if (workDuration.compareTo(REGULAR_WORK_DURATION) > 0) {
			workDuration = REGULAR_WORK_DURATION;
		}
		return workDuration;
	}
}
