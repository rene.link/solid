package com.link_intersystems.solid.srp.data_separation;

import static java.time.temporal.ChronoUnit.HOURS;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

import com.link_intersystems.solid.srp.TimeSheet;
import com.link_intersystems.solid.srp.WorkLog;

public class OvertimeReporter {

	public static final Duration REGULAR_WORK_DURATION = Duration.of(8, HOURS);

	private EmployeeData employeeData;

	public OvertimeReporter(EmployeeData employeeData) {
		this.employeeData = employeeData;
	}

	public BigDecimal reportOvertime(LocalDate from, LocalDate to) {
		BigDecimal overtime = getOvertimeHours(employeeData, from, to);
		return overtime;
	}

	private BigDecimal getOvertimeHours(EmployeeData employeeData, LocalDate from, LocalDate to) {
		BigDecimal overtimeHours = BigDecimal.ZERO;

		TimeSheet timeSheet = employeeData.getTimeSheet();
		List<WorkLog> workLogs = timeSheet.getWorkLogs(from, to);

		for (WorkLog workLog : workLogs) {
			Duration workDuration = workLog.getWorkDuration();
			Duration overtimeDuration = getOvertimeDuration(workDuration);
			long minutes = overtimeDuration.toMinutes();
			BigDecimal workLogHours = new BigDecimal(minutes).divide(new BigDecimal(60));
			overtimeHours = overtimeHours.add(workLogHours);
		}

		return overtimeHours;
	}

	private Duration getOvertimeDuration(Duration regularWorkDuration) {
		Duration overtimeDuration = regularWorkDuration.minus(REGULAR_WORK_DURATION);
		if (overtimeDuration.isNegative()) {
			overtimeDuration = Duration.ZERO;
		}
		return overtimeDuration;
	}

}
