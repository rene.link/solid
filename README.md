This repository contains the solid principle examples that I explain on my [you tube channel][0].

The solid principles have been created by [Robert C. Martin (aka. Uncle Bob)][1] in the 1990s and provide
us rules of how we should structure to produce high quality code.

The principles help us to get rid of 3 symptoms of bad code:

- **Rigidity** is due to the fact that a single change to heavily interdependent software begins a cascade
of changes in dependent modules. When the extent of that cascade of change cannot be predicted by the designers or maintainers, the impact of the change cannot be estimated.
This makes the cost of the change impossible to predict. Managers, faced with such unpredictability,
become reluctant to authorize changes. Thus the design becomes officially
rigid.
- **Fragility**: is the tendency of a program to break in many places when a single change is
made. Often the new problems are in areas that have no conceptual relationship with the
area that was changed. Such fragility greatly decreases the credibility of the design and
maintenance organization. Users and managers are unable to predict the quality of their
product. Simple changes to one part of the application lead to failures in other parts that
appear to be completely unrelated. Fixing those problems leads to even more problems,
and the maintenance process begins to resemble a dog chasing its tail.
- **Immobility** is when the desirable parts of the design are highly dependent
upon other details that are not desired. Designers tasked with investigating the design to
see if it can be reused in a different application may be impressed with how well the
design would do in the new application. However if the design is highly interdependent,
then those designers will also be daunted by the amount of work necessary to separate the
desirable portion of the design from the other portions of the design that are undesirable.
In most cases, such designs are not reused because the cost of the separation is deemed to
be higher than the cost of redevelopment of the design.


[0]: https://www.youtube.com/channel/UClrmCGWNiM1LllIuJ7kaKZw
[1]: https://blog.cleancoder.com/